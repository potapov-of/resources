app.controller('AjaxCtrl', ['$http','$scope', '$rootScope', function($http,$scope,$rootScope){
	$rootScope.$watch('search', function(val){
		$http({method:"GET", url:"/search", params: {q: val}}).success(function (res) {
			$scope.search = res;
		})
	})
}])