class Article < ActiveRecord::Base
	belongs_to :users
	validates :title, length: { minimum: 10 }
end
